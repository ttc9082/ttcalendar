from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Role(models.Model):
    title = models.CharField(max_length=20)
    allow = models.CharField(max_length=500)

    def __unicode__(self):
        return self.title


class Cuser(models.Model):
    user = models.OneToOneField(User, related_name='cuser', primary_key=True)
    role = models.ForeignKey(Role)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name


class Event(models.Model):
    title = models.CharField(max_length=500)
    description = models.TextField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    author = models.ForeignKey(Cuser)

    def __unicode__(self):
        return self.title



