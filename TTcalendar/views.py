from django.shortcuts import render
from .models import *
from datetime import datetime, timedelta
import json
from django.http import HttpResponse
import time
from django.core.context_processors import csrf
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/calendar/login')
def home(request):
    context = {}
    return render(request, 'home.html', context)

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/calendar')

def init_user(request):
    red = Role(title='red', allow='March,April,May,June')
    red.save()
    black = Role(title='black', allow='July,August,September,October')
    black.save()
    white = Role(title='white', allow='January,February,March,April,May,June,'
                                      ' July,August,September,October,November,December')
    white.save()
    r = User.objects.create_user(username='red', password='red')
    b = User.objects.create_user(username='black', password='black')
    w = User.objects.create_user(username='white', password='white')
    red_user = Cuser(user=r, role=red)
    red_user.save()
    black_user = Cuser(user=b, role=black)
    black_user.save()
    white_user = Cuser(user=w, role=white)
    white_user.save()
    test_event1 = Event(title='test1 title', start_time=datetime.now()+timedelta(days=-3, hours=-2),
          end_time=datetime.now()+timedelta(days=-3, hours=1),
          description='I guess it\'s just a test', author=red_user)
    test_event2 = Event(title='test2 title2', start_time=datetime.now()+timedelta(days=-2, hours=11),
          end_time=datetime.now()+timedelta(days=2, hours=1),
          description='I guess it\'s just a tes2t', author=white_user)
    test_event1.save()
    test_event2.save()
    context = {'users': Cuser.objects.all(), 'events': Event.objects.all()}
    return render(request, 'init.html', context)


def getEvent(request, month=False, nextmonth=False):
    if request.method == 'GET':
        if not month:
            date_string = request.GET['ttc']
            date_list = date_string.split(',')
            print date_list
            events = {}
            for date in date_list:
                dtime = datetime.fromtimestamp(int(date))
                events[date] = []
                print dtime
                # start time in day time range
                tmp = Event.objects.filter(start_time__gte=dtime).filter(start_time__lt=dtime+timedelta(days=1))
                for e in tmp:
                    tmp_e = {'author': e.author.user.pk, 'title': e.title,
                             'start': time.mktime(e.start_time.timetuple()),
                             'end': time.mktime(e.end_time.timetuple()), 'description': e.description, 'eid': e.pk}
                    events[date].append(tmp_e)
                # end time in day time range
                tmp = Event.objects.filter(end_time__gte=dtime).filter(end_time__lt=dtime+timedelta(days=1))
                for e in tmp:
                    tmp_e = {'author': e.author.user.pk, 'title': e.title,
                             'start': time.mktime(e.start_time.timetuple()),
                             'end': time.mktime(e.end_time.timetuple()), 'description': e.description, 'eid': e.pk}
                    if tmp_e not in events[date]:
                        events[date].append(tmp_e)
                # whole day in event range
                tmp = Event.objects.filter(start_time__lt=dtime).filter(end_time__gt=dtime+timedelta(days=1))
                for e in tmp:
                    tmp_e = {'author': e.author.user.pk, 'title': e.title,
                             'start': time.mktime(e.start_time.timetuple()),
                             'end': time.mktime(e.end_time.timetuple()), 'description': e.description, 'eid': e.pk}
                    if tmp_e not in events[date]:
                        events[date].append(tmp_e)
            return HttpResponse(json.dumps(events), content_type="application/json")
        else:
            month_event = []
            ms = datetime.fromtimestamp(int(month))
            me = datetime.fromtimestamp(int(nextmonth))
            # start time in day time range
            tmp = Event.objects.filter(start_time__gte=ms).filter(start_time__lt=me)
            for e in tmp:
                tmp_e = {'author': e.author.user.pk, 'title': e.title,
                         'startDate': e.start_time.strftime('%Y-%m-%d'),
                         'endDate': e.end_time.strftime('%Y-%m-%d'), 'description': e.description}
                month_event.append(tmp_e)
                print e
            # end time in day time range
            tmp = Event.objects.filter(end_time__gte=ms).filter(end_time__lt=me)
            for e in tmp:
                tmp_e = {'author': e.author.user.pk, 'title': e.title,
                         'startDate': e.start_time.strftime('%Y-%m-%d'),
                         'endDate': e.end_time.strftime('%Y-%m-%d'), 'description': e.description}
                if tmp_e not in month_event:
                    month_event.append(tmp_e)
                    print e, '1'
            # whole day in event range
            tmp = Event.objects.filter(start_time__lt=ms).filter(end_time__gt=me)
            for e in tmp:
                tmp_e = {'author': e.author.user.pk, 'title': e.title,
                         'startDate': e.start_time.strftime('%Y-%m-%d'),
                         'endDate': e.end_time.strftime('%Y-%m-%d'), 'description': e.description}
                if tmp_e not in month_event:
                    month_event.append(tmp_e)
                    print e, '2'
            return HttpResponse(json.dumps(month_event), content_type="application/json")


def createEvent(request):
    try:
        if request.method == 'POST':
            a = csrf(request)
            title = request.POST['title']
            des = request.POST['des']
            stime = request.POST['stime']
            etime = request.POST['etime']
            evt = []
            start_time = datetime.fromtimestamp(int(stime))
            end_time = datetime.fromtimestamp(int(etime))
            if int(etime) < int(stime):
                res = {'success': False, 'reason': 1, 'msg': 'End Time should greater than Start Time!'}
                return HttpResponse(json.dumps(res), content_type="application/json")
            tmp = Event.objects.filter(start_time__gte=start_time).filter(start_time__lt=end_time)
            for e in tmp:
                evt.append({'author': e.author.user.username, 'title': e.title, 'start': time.mktime(e.start_time.timetuple()),
                                         'end': time.mktime(e.end_time.timetuple()), 'description': e.description})
            tmp = Event.objects.filter(end_time__gte=start_time).filter(end_time__lt=end_time)
            for e in tmp:
                evt.append({'author': e.author.user.username, 'title': e.title, 'start': time.mktime(e.start_time.timetuple()),
                                         'end': time.mktime(e.end_time.timetuple()), 'description': e.description})
            tmp = Event.objects.filter(start_time__lt=start_time).filter(end_time__gt=end_time)
            for e in tmp:
                evt.append({'author': e.author.user.username, 'title': e.title, 'start': time.mktime(e.start_time.timetuple()),
                                         'end': time.mktime(e.end_time.timetuple()), 'description': e.description})
            print evt
            if evt:
                res = {'success': False, 'reason': 2, 'msg': 'Time conflict with ', 'conflict': evt}
                return HttpResponse(json.dumps(res), content_type="application/json")
            else:
                new_event = Event(title=title, description=des, start_time=start_time, end_time=end_time, author=request.user.cuser)
                new_event.save()
                res = {'success': True, 'reason': 0, 'msg': 'Event Created Successfully', 'conflict': None}
                return HttpResponse(json.dumps(res), content_type="application/json")
    except Exception, e:
        print e
        res = {'success': False, 'reason': 3, 'msg': 'Unknown Error', 'conflict': None}
        return HttpResponse(json.dumps(res), content_type="application/json")


def login(request):
    login_form = AuthenticationForm()
    context = {'login_form': login_form}
    context.update(csrf(request))
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        print username,password
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            print '1'
            return HttpResponseRedirect('/calendar')
        else:
            context['error'] = 'Sorry, Invalid input, Try again.'
            print '2'
            return render_to_response('login.html', context)
    print '3'
    return render_to_response('login.html', context)


def del_event(request, eid):
    Event.objects.get(id=eid).delete()
    return HttpResponseRedirect('/calendar')


def edi_event(request, eid):
    if request.method == 'POST':
        title = request.POST['title']
        des = request.POST['des']
        stime = request.POST['stime']
        etime = request.POST['etime']
        cur_ev = Event.objects.get(id=eid)
        c = {'author': cur_ev.author.user.username, 'title': cur_ev.title, 'start': time.mktime(cur_ev.start_time.timetuple()),
                                     'end': time.mktime(cur_ev.end_time.timetuple()), 'description': cur_ev.description}
        evt = []
        start_time = datetime.fromtimestamp(int(stime))
        end_time = datetime.fromtimestamp(int(etime))
        if int(etime) < int(stime):
            res = {'success': False, 'reason': 1, 'msg': 'End Time should greater than Start Time!'}
            return HttpResponse(json.dumps(res), content_type="application/json")
        tmp = Event.objects.filter(start_time__gte=start_time).filter(start_time__lt=end_time)
        for e in tmp:
            evt.append({'author': e.author.user.username, 'title': e.title, 'start': time.mktime(e.start_time.timetuple()),
                                     'end': time.mktime(e.end_time.timetuple()), 'description': e.description})
        tmp = Event.objects.filter(end_time__gte=start_time).filter(end_time__lt=end_time)
        for e in tmp:
            evt.append({'author': e.author.user.username, 'title': e.title, 'start': time.mktime(e.start_time.timetuple()),
                                     'end': time.mktime(e.end_time.timetuple()), 'description': e.description})
        tmp = Event.objects.filter(start_time__lt=start_time).filter(end_time__gt=end_time)
        for e in tmp:
            evt.append({'author': e.author.user.username, 'title': e.title, 'start': time.mktime(e.start_time.timetuple()),
                                     'end': time.mktime(e.end_time.timetuple()), 'description': e.description})
        print evt
        evt.remove(c)
        if evt:
            res = {'success': False, 'reason': 2, 'msg': 'Time conflict with ', 'conflict': evt}
            return HttpResponse(json.dumps(res), content_type="application/json")
        else:
            cur_ev.title = title
            cur_ev.author = request.user.cuser
            cur_ev.start_time = start_time
            cur_ev.end_time = end_time
            cur_ev.description = des
            cur_ev.save()
            res = {'success': True, 'reason': 0, 'msg': 'Event Edited Successfully', 'conflict': None}
            return HttpResponse(json.dumps(res), content_type="application/json")
    return HttpResponseRedirect('/calendar')