/**
 * Created by TTc9082 on 7/19/14.
 */


function getEventsFromMonth(month){
    var cur_month = month.unix();
    var nxt_month = month.endOf('month').unix();
    $.getJSON('/calendar/getEvent/'+cur_month+'/'+nxt_month+'/', {},function(data){
        console.log(data);
        s = data;
        cal1.addEvents(data);
    });
//    this.options.events = [];
}

function createNewEvent(target){
    $('#create_modal').modal('show');
    $('#create_modal .modal-title').html('<span class=\"glyphicon glyphicon-paperclip\"></span>  New Event '+target.date.format('dddd YYYY-MM-DD'));
    $('#create_modal #stime').val(target.date.format('YYYY-MM-DD 09:00'));
    $('#create_modal #etime').val(target.date.format('YYYY-MM-DD 12:00'));
}



function queryEvents(){
    var result = "";
    $('#events_list').empty();
    $( ".ui-selected", this ).each(function() {
        var day = this;
        var classNameIndex = day.className.indexOf('calendar-day-');
        if(classNameIndex !== 0) {
        // our unique identifier is always 23 characters long.
        // If this feels a little wonky, that's probably because it is.
        // Open to suggestions on how to improve this guy.
        dateString = day.className.substring(classNameIndex + 13, classNameIndex + 23);
        var date = moment.utc(dateString).unix();
        }
        if(result){result+=','+date;}
        else{result=date;}
    });
    console.log(result);
    $.getJSON('/calendar/getEvent/', {'ttc':result}, function(data){
//        alert('cool.');
        $.each(data, function(key, value){
           appendPanel(key, value);
        });
    }).done(function( data ) {
    $('.ed').on('click', function(element){
        var ev = $(this).parent().parent();
        var edibutn = $('#editEvent');
        $('#edit_modal').modal('show');
        edibutn.attr('href', 'edi/'+ev.attr('data-eid'));
        $('#edit_modal .modal-title').html('<span class=\"glyphicon glyphicon-paperclip\"></span>  Edit Event');
        $('#edit_modal #stime').val(moment.unix(ev.attr('data-st')).format('YYYY-MM-DD HH:mm'));
        $('#edit_modal #etime').val(moment.unix(ev.attr('data-et')).format('YYYY-MM-DD HH:mm'));
        $('#edit_modal #event_title').val(ev.attr('data-tt'));
        $('#edit_modal #event_description').val(ev.attr('data-ds'));
        edibutn.on('click', function(e){
            console.log('edit!');
            var title = $('#edit_modal #event_title').val();
            var des = $('#edit_modal #event_description').val();
            var start_time = moment($('#edit_modal #stime').val()).unix();
            var end_time = moment($('#edit_modal #etime').val()).unix();
            var csrftoken = $.cookie('csrftoken');
            $.ajax('edi/'+ev.attr('data-eid')+'/',$.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                data: {'title':title, 'des':des, 'stime':start_time, 'etime': end_time},
                type: 'POST'
                })).done(function( data ) {
                    if ( console && console.log ) {
                        var msg = $("#msg");
                        msg.css('display', 'none');
                        var form = $('.form-group');
                        form.removeClass('has-success').removeClass('has-danger');
                        var time_form = $('.time_form');
                        console.log(data);
                        getEventsFromMonth(cal1.month);
                        if(data['success']){
                            form.addClass('has-success');
                            msg.addClass('success');
                            $('.modal').modal('hide');
                            form.removeClass('has-success has-feedback');
                        }
                        else{
                            if(data['reason'] == 2){msg.text(data['msg']);time_form.addClass('has-error');}
                            if(data['reason'] == 1){msg.text(data['msg']);time_form.addClass('has-warning');}
                            msg.css('display', 'block')
                        }
                    }


            });
            });
    });
    $('.de').on('click', function(element){
        $('#del_modal').modal('show');
        var id = $(this).attr('data-eid');
        $('#del_button').attr('href', 'del/'+$(this).parent().parent().attr('data-eid'));
    });
     $('.dt').on('click', function(element){
        $('#det_modal').modal('show');
        var ev = $(this).parent().parent();
        $('#det_modal h2').text('Event Title: ' + ev.attr('data-tt'));
        $('#det_modal #2').text('Description: ' + ev.attr('data-ds'));
        $('#det_modal #3').text('From '+ moment.unix(ev.attr('data-st')).format('YYYY-MM-DD HH:mm')+ ' To '+ moment.unix(ev.attr('data-et')).format('YYYY-MM-DD HH:mm'));
    });
    });

}

function appendPanel(date, events){
    var event_li = '';
    events.forEach(function (element){
       event_li+='<div class="row" ' +
                 'data-eid="'+element['eid']+
                 '" data-st="'+element['start']+
                 '" data-et="'+element['end']+
                 '" data-tt="'+element['title']+
                 '" data-ds="'+element['description']+
                 '" data-au="'+element['author']+'">'+
                 '<div class="col-md-6">' + element['title']+ '</div>'+
                 '<div class="col-md-2 del"><button type="button" class="dt btn btn-info btn-xs btn-block">Detail</button></div>';
       if($('#role').attr('data-role') == 'white' || $('#pk').attr('data-pk') == element['author']){
           event_li+='<div class="col-md-2 edt"><button type="button" class="ed btn btn-success btn-xs btn-block">Edit</button></div>'+
                     '<div class="col-md-2 del"><button type="button" class="de btn btn-danger btn-xs btn-block">Del</button></div>' + '</div>'
       }
        else{
           event_li+='<div class="col-md-2 edt"><button type="button" class="btn btn-success btn-xs btn-block disabled">Edit</button></div>'+
                     '<div class="col-md-2 del"><button type="button" class="btn btn-danger btn-xs btn-block disabled">Del</button></div>' + '</div>'
       }
});
    if (!event_li){event_li='<div class="row" style="text-align: center; color: grey;">No Event</div>'}
    var event_panel = '<div class="panel panel-default">'+
            '<div class="panel-heading">'+ moment.unix(parseInt(date)).utc().format('Do dddd') +'</div>'+
            '<div class="panel-body">' + event_li +
            '</div>';
    $(event_panel).appendTo("#events_list");
}

