# A simple information system with Calendar UI #

Functionality Requirements

1. Design a login process for the following three users(roles)

    * Red
    * Black
    * White

2. User will have their own homepage (a same page) with different content and actions based on their authorities, see below

3. On the homepage, display an interactive calendar

    * Calendar could be displayed with different views: Monthly/Weekly/Daily
    * Show the CSS styles of different calendar days: current date/selected date/out of month date/blocked dates ( see 4.c.v)

4. Interaction on the calendar 

    * Drag and drop to select day(s) on the calendar
    * Show the related events in a list view based on those days selected above

        * i.	Get the result from database (SQL Server + LINQ to SQL)
        * ii.	Search and displayed asynchronous

    * Double click on any of day in the calendar to pop up a dialog to add event

        * i.	Event information include:

            * Title
            * Description
            * Start time/End time

        * ii.	Selected date should be auto populated into the value of Star time /End time
        * iii.	The data should be saved into database
        * iv.	If the time slot is occupied by other events, it is not able to be selected
        * v.	If the time is blocked, it is not able to be selected

            1.	Red – allow March to June
            2.	Black – allow July to October
            3.	White – allow to all 

    * For the result list

        * a.	All users could search out and view all the events
        * b.	“Red” and “Black” could only edit and delete the events created by themselves
        * c.	“White” could edit and delete all events

            1. The result should be saved into database


Notes:	
	
1. You need set up a web application solution to run the whole project
1. You could just test it out in Google Chrome
1. You could refer to third-party calendar controls, such as Google Calendar
