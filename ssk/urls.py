from django.conf.urls import patterns, include, url
import TTcalendar
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ssk.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^calendar/$', 'TTcalendar.views.home'),
    url(r'^calendar/init', 'TTcalendar.views.init_user'),
    url(r'^calendar/getEvent/$', 'TTcalendar.views.getEvent'),
    url(r'^calendar/getEvent/(?P<month>\d+)/(?P<nextmonth>\d+)/$', 'TTcalendar.views.getEvent'),
    url(r'^calendar/createEvent', 'TTcalendar.views.createEvent'),
    url(r'^calendar/login/$', 'TTcalendar.views.login', name='login'),
    url(r'^calendar/logout/$', 'TTcalendar.views.logout'),
    url(r'^calendar/del/(?P<eid>\d+)/$', 'TTcalendar.views.del_event'),
    url(r'^calendar/edi/(?P<eid>\d+)/$', 'TTcalendar.views.edi_event'),
    url(r'^admin/', include(admin.site.urls)),
)
